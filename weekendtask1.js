let c=["bana","apple","grape"];    //accessing array elements

for (let index = 0; index < c.length; index++) {
 // console.log(c);
  //console.log(c[index]);
  
}console.log(c);
c[0]="mango";  //changing array elements
console.log(c[0]);
console.log(c.length); //length of array
console.log(c[c.length -1]);
let a=typeof c;  //to recognize an array
console.log();
let b=Array.isArray(c); //
console.log(b);
c.sort();

console.log(c); //to sort an array
let rev=c.reverse();//to sort in reverse order
console.log(rev);
let e=[10,20,30];
 let f=c.concat(e);
console.log(f);//to add two arrays
f.splice(3,2); //to remove specified elements
console.log(f);
f.splice(2,2,15,45);//splicevis also used to insrt an elements
console.log(f);
let f1=f.toString();
console.log(f1);//converting array to string
let f2=f.join();
console.log(f2);
let f3=[10,20,30,40];//to craete new array with slice
let f4=f3.slice(1);
console.log(f4);
let f5=f3.slice(1,3);
console.log(f5);
let myObj = { "name":"John", "age":30, "car":null };
let x=myObj.name;   //accessing objects
console.log(x);
let y=myObj["age"];
console.log(y);
var dictionary = {"data":[{"id":"0","name":"ABC"},{"id":"1", "name":"DEF"}], "images": [{"id":"0","name":"PQR"},{"id":"1","name":"xyz"}]};
for (item in dictionary) {
  for (subItem in dictionary[item]) {
     console.log(dictionary[item][subItem]);
  }
}