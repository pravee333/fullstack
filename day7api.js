fetch('https://api.themoviedb.org/3/movie/popular?api_key=99c26306be3d1ce05090f2450ff90c6e&language=en-US&page=1').then(function (res) { // Fetching the data from API Url, .then is used to say execute this when this is done.
    //console.log(res);
    return res.json();
}).then(function (result) {
    console.log(result);
    
    newMovies(result);
});


function newMovies(data) {
    let myId = document.getElementById("myId");
    for (let i = 0; i < data.results.length; i++) {
        
        let src="https://image.tmdb.org/t/p/w185_and_h278_bestv2";
        let src1=data.results[i].poster_path;
        let src2=src.concat(src1);
        let myHTML = '<div class="parentDiv">\
        <img src = "' + src2+'" > \
        <div class = "title" > ' + data.results[i].title + ' </div>\
        <div class="date">' + data.results[i].release_date+ ' </div>\
        <p class= "class1"> ' + data.results[i].overview + ' </p>\
    </div>';
        myId.innerHTML += myHTML;

    }
}
